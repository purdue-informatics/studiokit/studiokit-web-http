﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.ErrorHandling.WebApi;
using System;
using System.Net;

namespace StudioKit.Web.Http
{
	/// <summary>
	/// Represents an ActionResult that returns an <see cref="ApiErrorMessage" />.
	/// </summary>
	public class ApiErrorMessageResult : ActionResult
	{
		private readonly HttpStatusCode _statusCode;
		private readonly string _title;
		private readonly string _message;

		public ApiErrorMessageResult(HttpStatusCode statusCode, string title = null, string message = null)
		{
			_statusCode = statusCode;
			_title = title;
			_message = message;
		}

		public override void ExecuteResult(ActionContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException(nameof(context));
			}

			var response = context.HttpContext.Response;
			response.StatusCode = (int)_statusCode;
			var errorMessage = new ApiErrorMessage(_statusCode, _title, _message);
			new JsonResult(errorMessage).ExecuteResult(context);
		}
	}
}