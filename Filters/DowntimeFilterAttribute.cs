﻿using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using StudioKit.Configuration;
using StudioKit.Encryption;
using System.Net;
using System.Threading.Tasks;

namespace StudioKit.Web.Http.Filters
{
	public class DowntimeFilterAttribute : AuthorizeFilter
	{
		public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
		{
			var value = EncryptedConfigurationManager.GetSetting(BaseAppSetting.IsDowntime);
			if (string.IsNullOrWhiteSpace(value))
			{
				await base.OnAuthorizationAsync(context);
				return;
			}

			var isDowntime = bool.Parse(value);
			if (isDowntime)
			{
				context.Result = new ApiErrorMessageResult(
					HttpStatusCode.Forbidden,
					"We'll be right back!",
					"Our developers are currently hard at work. Normal service will be restored soon.");
				return;
			}

			await base.OnAuthorizationAsync(context);
		}
	}
}