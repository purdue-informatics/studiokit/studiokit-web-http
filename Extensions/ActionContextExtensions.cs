﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;

namespace StudioKit.Web.Http.Extensions
{
	public static class ActionContextExtensions
	{
		public static bool IsPrincipalAuthenticated(this ActionContext actionContext)
		{
			var identity = (ClaimsIdentity)actionContext.HttpContext.User.Identity;
			return identity?.IsAuthenticated == true;
		}

		public static bool IsPrincipalAuthenticatedUser(this ActionContext actionContext)
		{
			var identity = (ClaimsIdentity)actionContext.HttpContext.User.Identity;
			return identity?.IsAuthenticated == true && identity.Claims.Any(c => c.Type == ClaimTypes.Email);
		}
	}
}